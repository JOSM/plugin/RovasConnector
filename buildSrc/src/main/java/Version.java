public final class Version {
  public static final String AWAITILITY = "4.2.1";
  public static final String GRADLE_JOSM_PLUGIN = "0.8.2";
  public static final String JUNIT = "5.10.3";
  public static final String JUNIT4 = "4.13.2";
  /**
   * Align this version with <a href="https://github.com/codeclimate/codeclimate-pmd/blob/channel/beta/bin/install-pmd.sh">
   *   what is available as codeclimate plugin
   * </a>.
   */
  public static final String PMD = "6.40.0";
  public static final String GRADLE_SPOTBUGS_PLUGIN = "6.0.19";
  public static final String SPOTBUGS = "4.8.6";
  public static final String WIREMOCK = "3.9.1";
}
